package main

import (
	"fmt"
	"net"
	"os"
	"runtime"
	"strings"
)

const (
	serverIP   = "localhost"
	serverPort = 5006
)

var (
	clients []net.Conn
	//debug                         bool = true
	Version, BuildDate, GitCommit string
)

func main() {
	fmt.Printf("TCP Thingy - %s - %s\n", runtime.Version(), GitCommit)
	fmt.Println("Starting...")

	addr := net.TCPAddr{
		IP:   net.ParseIP(serverIP),
		Port: serverPort}

	server, err := net.ListenTCP("tcp", &addr)
	check(err)

	defer server.Close()

	//clients = make([]net.Conn, 0)

	// Handle new connections on a goroutine
	for {
		nc, err := server.Accept()
		if err != nil {
			fmt.Printf("Error: %s already died?\n", nc.RemoteAddr())
			continue
		}

		if nc != nil {
			clients = append(clients, nc)
			sendToAll([]byte("New connection\n"), nc.RemoteAddr())
			fmt.Printf("[connect] %s - %d clients total\n", nc.RemoteAddr(), len(clients))
		}

		go handleSock(nc)

	}

}

func handleSock(conn net.Conn) {
	buf := make([]byte, 1024)
	for {
		size, err := conn.Read(buf)
		if err != nil {
			fmt.Printf("[died] remote %s died @ handleSock\n", conn.RemoteAddr())
			conn.Close()
			removeSock(conn)
			break
		}
		str := string(buf[0:size])
		fmt.Printf("[me <- %s] %d bytes\n", conn.RemoteAddr(), size)
		sendToAll([]byte(str), conn.RemoteAddr())
	}
}

func removeSock(c net.Conn) {
	fmt.Printf("[remove] %s\n", c.RemoteAddr())
	var i int
	for i = range clients {
		if clients[i] == c {
			break
		}
	}
	fmt.Printf("[disconnect] removing %s at pos %d... %d clients now\n", c.RemoteAddr(), i, len(clients))
	clients = append(clients[:i], clients[i+1:]...)
	sendToAll([]byte("Someone left\n"), c.RemoteAddr())
}

func sendToAll(b []byte, not net.Addr) {
	for _, client := range clients {
		if client.RemoteAddr() == not {
			continue
		}
		written, err := client.Write(b)
		fmt.Printf("[me -> %s] %d bytes\n", client.RemoteAddr(), written)
		if err != nil {
			fmt.Printf("[died] remote %s died @ sendToAll\n", client.RemoteAddr())
			client.Close()
			removeSock(client)
		}
	}
}

func check(err error) {
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		os.Exit(1)
	}
}

func clean(s string) string {
	return strings.Trim(s, "\n")
}
