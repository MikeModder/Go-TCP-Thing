package main

import (
	"fmt"
	"net"
	"os"
	"runtime"
)

const (
	addr = "localhost"
	port = 10009
)

func main() {
	fmt.Printf("Test Client - %s\n", runtime.Version())
	fmt.Printf("Trying to connect to server @ %s port %d\n", addr, port)

	address := net.UDPAddr{
		Port: port,
		IP:   net.ParseIP(addr)}

	conn, err := net.DialUDP("udp", nil, &address)
	check(err)

	in := make([]byte, 1024)
	//out := make([]byte, 1024)
	sendMe := []byte("SAMPLE")

	fmt.Printf("Connected to %s as %s\n", conn.RemoteAddr(), conn.LocalAddr())

	conn.Write(sendMe)
	len, err := conn.Read(in)
	check(err)

	if string(in[0:len]) != "SAMPLE" {
		fmt.Println("Error! Expected SAMPLE but got %s\n", string(in[0:len]))
		os.Exit(1)
	} else {
		fmt.Printf("Server responded with %s, pass\n", string(in[0:len]))
		os.Exit(0)
	}

}

func check(err error) {
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		os.Exit(1)
	}
}
