# What is this?
Me playing around with TCP sockets in Golang.

# Why?
Because I wanted to play around with Go and TCP sockets

# Is this useful?
Maybe? I might make it conform to some protocall eventually, but for now it's just me playing around with Golang and sockets

# Did I mention?
Golang and sockets?